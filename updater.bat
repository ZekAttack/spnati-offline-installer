@echo off
@cls
set GIT=http://github.com/git-for-windows/git/releases/download/v2.17.1.windows.2/PortableGit-2.17.1.2-32-bit.7z.exe
set NWJSVER=nwjs-v0.31.2-win-ia32
set NWJS=http://dl.nwjs.io/v0.31.2/%NWJSVER%.zip
cd /d %~dp0
if exist 7za.exe (
    wget -O nwjs.zip %NWJS%
    7za x nwjs.zip
    del nwjs.zip
    rename %NWJSVER% spnati
    cd spnati
    where git.exe
    if errorlevel 1 (
        mkdir git
        cd git
        ..\..\wget -O git.7z.exe %GIT%
        ..\..\7za x git.7z.exe
        del git.7z.exe
        call post-install.bat
        cd ..
    )
    rename nw.exe spnati.exe
)
PATH %CD%\git\cmd;%PATH%
echo Updating Strip Poker Night at the Inventory.
IF EXIST package.nw\.git (
    echo Found existing installation, will download only new changes.
    cd package.nw
    git reset --hard origin/master
    git pull
) ELSE (
    echo No existing installation found, will download everything.
    echo This will likely take a long time, but subsequent updates will be faster.
    git clone https://gitlab.com/spnati/spnati.gitlab.io.git package.nw
    echo {"name":"Strip Poker Night at the Inventory","main":"index.html"} > package.nw\package.json
)
if exist ..\7za.exe (
    cd ..
    del 7za.exe
    del wget.exe
    move README.md spnati\README.txt
    move LICENSE spnati\LICENSE.txt
    move updater.bat spnati\updater.bat
) else (
    echo Update finished. Review the result above and...
    pause
)
